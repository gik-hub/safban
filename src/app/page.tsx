'use client';
import React from 'react';
import { Grid, Stack } from '@mui/material';
import KanbanColumn from '@/components/kanban-column';
import { AddComment } from '@mui/icons-material';
import AddComponent from '@/components/add-component';

const works = [
  {
    state: 'To do',
    tasks: [
      {
        description: 'task 1 description',
      },
      {
        description: 'task 1 description',
      },
      {
        description: 'task 1 description',
      },
    ],
  },
  {
    state: 'In progress',
  },
];

function Safban() {
  return (
    <Stack direction={'row'} spacing={2} alignItems={'flex-start'} mt={2}>
      {works.map((work) => (
        <KanbanColumn key={work?.state} work={work} />
      ))}
      <Stack width={'20%'}>
        <AddComponent btnLabel={'Add column'} />
      </Stack>
    </Stack>
  );
}

export default Safban;
